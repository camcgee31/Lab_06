#!/bin/bash

#Simple script that relaces any words inside of the listed movies below
#To run hust type sh moviescraper.sh
#Note you will get file outputs

#get user input
echo "Pick out of the 2 movies type the number"
echo "1:)bee movie"
echo "2:Shrek 1  "
read pick

#both websites made it easy to pull the scripts
urlbee="https://meme.fandom.com/wiki/Bee_Movie_Script"
urlshrek="https://shrek.fandom.com/wiki/Shrek_(film)/Transcript"

# If pick is 1 then it will pull the bee movie
if [ "$pick" = "1" ] 
then

	echo "What word would you like to replace?"
	read reword

	echo "What word would you like to change it to?"
	read newword
	#Since the bee movie was all on one line all I needed to do was to pull the first word from it and then it gave me the rest
	curl -s -dump "$urlbee" | grep -i "<p>According" | sed "s/$reword/$newword/gI" > beemoive.html

	echo "Outputed to beemoive.html"
# If pick is 2 then it will pull the shrek movie
elif [ "$pick" = "2" ]
then

	echo "What word would you like to replace?"
	read reword

	echo "What word would you like to change it to?"
	read newword
	# all of the script was in a </p><p> for each line so I just greped that to get all the words.
	curl -s -dump "$urlshrek" | grep -i "</p><p>" | sed "s/$reword/$newword/gI" > shrek.html
	
	echo "Output file shrek.html"

fi

